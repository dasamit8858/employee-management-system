import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss']
})
export class EmployeeEditComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }
  employeeForm : FormGroup;
  Employeeid: Number;
  Name: String = '';
  Dateofbirth: Date;
  Salary: Number;
  Skills: Array<number> = [];

  ngOnInit() {
  this.employeeForm = this.formBuilder.group({
    'Employeeid' : [null, Validators.required],
    'Name' : [null, Validators.required],
    'Dateofbirth' : [null, Validators.required],
    'author' : [null, Validators.required],
    'Salary' : [null, Validators.required],
    'Skills' : [null, Validators.required]
  });
}
getEmployee(id) {
  this.api.getEmployee(id).subscribe(data => {
    this.Employeeid = data._id;
    this.employeeForm.setValue({
      Name: data.Name,
      Dateofbirth: data.Dateofbirth,
      description: data.description,
      Salary: data.Salary,
      Skills: data.Skills
    });
  });
}
onFormSubmit(form:NgForm) {
  this.api.updateEmployee(this.Employeeid)
    .subscribe(res => {
        let id = res['_id'];
        this.router.navigate(['/employee-details', id]);
      }, (err) => {
        console.log(err);
      }
    );
}
EmployeeDetails() {
  this.router.navigate(['/employee-details', this.Employeeid]);
}

}
