import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
declare let $:any;
@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.scss']
})
export class EmployeeCreateComponent implements OnInit {

  constructor(private router: Router, private api: ApiService,private formBuilder: FormBuilder) { }
  employeeForm : FormGroup;
  Employeeid: Number;
  Name: String = '';
  Dateofbirth: Date;
  Salary: Number;
  Skills: String;
  

  employee: any = {};

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      'Dateofbirth':  ['',Validators.compose([Validators.required])],
      'Name': ['',Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z]+$')])],
      'Skills': ['',Validators.compose([Validators.required])]
    })
    this.employee = this.api.selected_employee;
    if(!this.employee){
      this.employee = {};
    }
  }
  
  readURL(event:any) {
    console.log(event);

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
  
      reader.onload = (event: ProgressEvent) => {
        this.employee.ImageUpload = (<FileReader>event.target).result;
      }
  
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  validateName(event:any) {
    var regex_name  = /^[a-zA-Z ]*$/;
    if(!regex_name.test(event.target.value)) {
      event.preventDefault();
    }
  }
  /*validateSkill(event:any) {
    var regex_skill = 
  }*/
createEmployee() {
  console.log(this.employee);
  this.api.postEmployee(this.employee)
    .subscribe(res => {
        let id = res['_id'];
        this.router.navigate(['/']);
      }, (err) => {
        console.log(err);
      });
}

updateEmployee() {
  this.api.updateEmployee(this.employee)
    .subscribe(res => {
        let id = res['_id'];
        this.router.navigate(['/']);
      }, (err) => {
        console.log(err);
      });
}

}
