import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  employees: any;
  search_inp: string;
  eid: number = 100;
  constructor(private api: ApiService, private router: Router) { }
  //displayedColumns = ['EmployeeId', 'Name', 'Dateofbirth','Salary','Skills'];
  //dataSource = new EmployeeDataSource(this.api);

  searchEmployee(): void{
    this.api.getEmployee(this.search_inp)
    .subscribe(res => {
      console.log(res);
      this.employees = res;
    }, err => {
      console.log(err);
    });
  }

  selectEmployee(e): void{
    this.api.selected_employee = e;
    this.router.navigateByUrl('/employee-detail');
  }

  ngOnInit() {
    this.api.selected_employee = {};
    this.api.getEmployees()
    .subscribe(res => {
      console.log(res);
      this.employees = res;
    }, err => {
      console.log(err);
    });
  }

}
export class EmployeeDataSource extends DataSource<any> {
  constructor(private api: ApiService) {
    super()
  }

  connect() {
    return this.api.getEmployees();
  }

  disconnect() {

  }
}
