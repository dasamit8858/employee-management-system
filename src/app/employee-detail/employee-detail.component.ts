import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private api: ApiService,private router: Router) { 
    //this.getEmployeeDetails(this.route.snapshot.params['id']);
  }
  employee = {}
  ngOnInit() {
    this.employee = this.api.selected_employee;
    console.log(this.api.selected_employee);
  }
  getEmployeeDetails(id) {
    this.api.getEmployee(id)
      .subscribe(data => {
        console.log(data);
        this.employee = data;
      });
  }
  deleteEmployee(id) {
    this.api.deleteEmployee(id)
      .subscribe(res => {
          this.router.navigate(['/']);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
