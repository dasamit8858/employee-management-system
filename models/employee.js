var mongoose = require('mongoose')

var employeeSchema = new mongoose.Schema({
    Employeeid: String,
    Name: String,
    DateOfBirth: String,
    Salary: Number,
    Skills: String,
    ImageUpload:String
})

module.exports = mongoose.model('Employee',employeeSchema);