var express = require('express');
var router = express.Router();

var Employee = require('../models/employee');

var ValidateInput = require('../controllers/validate')
//get all the employee details
router.get('/',(req,res,next)=>{
    console.log("get request successfull!!");
    //res.send("get request successfull!!");
    Employee.find({},function(err,details) {
        if(err) return next(err);
        res.json(details);
    })
    //res.send('hi!! connected!!');
    
});

//get employee by name
router.get('/:name',(req,res,next)=>{
    console.log("get employee by name: ",req.params.name);
    Employee.find({"Name":{$regex:req.params.name,$options:'i'}},function(err,details){
        if(err) return next(err);
        return res.json(details);

    })
})

// Save an Employee
router.post('/',(req,res,next)=>{
    var return_msg = ValidateInput(req);
    console.log(return_msg);
    if(return_msg === "OK") {
        Employee.create(req.body,function(err,details){
            if(err) return next(err);
            res.json(details)
        })
    }
    else {
        res.send(return_msg);
    }
})

//Update an Employee
router.put('/',(req,res,next)=>{
    var return_msg = ValidateInput(req);
    if(return_msg === "OK") {
        console.log("inside update method: ",req.body);
        Employee.findByIdAndUpdate(req.body._id,req.body,function(err,details){
            if(err) return next(err);
            res.json(details)
        })
    }
    else {
        res.send(return_msg)
    }

})

//Delete an Employee
router.delete('/:id',(req,res,next)=>{
    console.log("inside delete method: ",req.body);
    Employee.findByIdAndRemove(req.params.id,req.body,function(err,details){
        if(err) return next(err);
        res.json(details)
    })
})

module.exports = router;